/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 *
 * @author Junyang
 * 
 */
//TODO Exercise 1.3 Step 1 Please refer tutorial exercise. 
public class Property {
    // A number that is used to identify a unique property.
	private int propertyId;
	// Address of property
	private String address;
	// The number of bedrooms on the property.
	private int numberOfBedrooms;
	// property's size
	private double size;
	// property's price
	private double price;
	
	/**
	 * Default constructor
	 */
	public Property(){
		this.propertyId = 0;
		this.address = " ";
		this.numberOfBedrooms = 0;
		this.size = 0.00;
		this.price = 0.00;	
	}	
	
	public Property(Property property) {
		this.propertyId = property.propertyId;
		this.address = property.address;
		this.numberOfBedrooms = property.numberOfBedrooms;
		this.size = property.size;
		this.price = property.price;
	}
	/**
	 * non-Default constructor
	 */
	public Property(int propertyId, String address, int numberOfBedrooms, double size, double price){
		this.propertyId = propertyId;
		this.address = address;
		this.numberOfBedrooms = numberOfBedrooms;
		this.size = size;
		this.price = price;
	}
	
	public int getPropertyId() {
		return propertyId;
	}
	
	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}
	
	public int getNumberOfBedrooms() {
		return numberOfBedrooms;
	}
	
	public void setNumberOfBedrooms(int numberOfBedrooms) {
		this.numberOfBedrooms = numberOfBedrooms;
	}
	
	public String address() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public double getSize() {
		return size;
	}
	
	public void setSize(double size) {
		this.size = size;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String toString() {
		return propertyId + ": " + address + " " + numberOfBedrooms + "BR(s)" + size + " sqm"
				+ " $" + price;
	}
}
